import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export default new Vuex.Store({
    state:{
        temp:0,
        hum:0
    },
    mutations:{
        incTemp:function (state, val) {
            state.temp = val;
        }
    }
})